

function createCard(name, description, pictureUrl, starts, ends, subtitle) {
    return `
        <div class="card"
        <div class="shadow-lg p-3 mb-5 bg-body rounded"></div>
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class = "class-subtitle mb-2 text-muted">${subtitle}</h6>
            <p class="card-text">${description}</p>
        </div>
        </div>
        <ul class="list-group list-group flush">
            <li class = "list-group-item">${starts} - ${ends}</li>
        </ul>
    `;
}
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json();
            let count = 0
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts).toLocaleDateString();
                    const ends = new Date(details.conference.ends).toLocaleDateString();
                    const subtitle = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl, starts, ends, subtitle);
                    const column = document.querySelector(`.col.num${count % 3}`);
                    column.innerHTML += html;
                    count++
                }
            }

        }
    } catch (e) {
        console.error(e);
        // Figure out what to do if an error is raised
    }
    // Figure out what to do if an error is raised
}

);
